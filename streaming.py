import re
import time
import copy
import threading
import codecs
import pandas as pd
import tweepy
import json
import numpy as np
from pytz import timezone
from datetime import datetime
import requests
from urllib3.exceptions import ReadTimeoutError
import psycopg2
from datetime import timedelta
from urllib.request import urlopen

from   tweepy.api import API

# Special Exceptions
from requests.exceptions import Timeout
from requests.exceptions import ConnectionError
#from requests.packages.urllib3.exceptions import ReadTimeoutError

prevAvgSent = {}
averageDiff = {}
differences = {}
sentimentGroup = {}
stockTrendGroup = {}
prices = {}
old  = {}
oldPrices = []
oldPredictions = []
sentimentTenMinDict = {}
prevSent = 0
prevPrice = 0
priceTenMinDict = {}
maxVal = 0
minVal = 0
predictionLast = {}

def getTracker(coins):
    tracker = {'Volume': {}, 'Sentiment': {},'Stock': {},'timestamp': {},'Seconds': 0, 'Trend': {}}
    for coin in coins:
        tracker['Volume'][coin[0]] = 0
        tracker['Stock'][coin[0]] = 0
        tracker['timestamp'][coin[0]] = 0
        tracker['Sentiment'][coin[0]] = {}
        tracker['Sentiment'][coin[0]]['all'] = []
        tracker['Sentiment'][coin[0]]['retweets']=[]
        tracker['Sentiment'][coin[0]]['followers']=[]
        tracker['Sentiment'][coin[0]]['avg'] = 'N/A'
        tracker['Sentiment'][coin[0]]['avg2'] = 'N/A'
        tracker['Trend'][coin[0]] = 0
        
    return tracker


def getReversal(coins):
    reversal = {}
    for coin in coins:
        for term in coin:
            reversal[term] = coin[0]
    return reversal



def get_trend(newPrice, coin, numCoins):
        
    global differences
    global prices
    global maxVal
    global minVal
    ticker = coin[0]



    maxVal = 0
    minVal = 0
    rtn = 0


    
    if len(prices) < numCoins:
        prices[ticker] = []
        differences[ticker] = []
        averageDiff[ticker] = []

    tickerPrices = prices[ticker]
    pricesLen = len(tickerPrices)

    if pricesLen == 10:
        price_diff = newPrice - tickerPrices[pricesLen-1]
        ave_diff = price_diff
        difference = abs(price_diff)

        prices[ticker].append(newPrice)
        prices[ticker] = prices[ticker][1:]
        averageDiff[ticker].append(ave_diff)
       
        differences[ticker].append(difference)
        
        for dif in differences[ticker]:
            maxVal += dif

        
        minVal = maxVal*(-1)


        try:
            rtn = (2*((difference - minVal)/(maxVal - minVal))-1)
        except: ZeroDivisionError

        if len(differences[ticker]) == 10:
            differences[ticker] = differences[ticker][1:]


        return rtn 
    else:
        if pricesLen != 0:

            price_diff = newPrice - tickerPrices[pricesLen-1]
            difference = abs(price_diff)
            ave_diff = price_diff
            averageDiff[ticker].append(ave_diff)
            if newPrice != 0:
                prices[ticker].append(newPrice)
            if tickerPrices[pricesLen-1] != 0:
                differences[ticker].append(difference)

            return 0    
        else:
            if newPrice != 0:
                prices[ticker].append(newPrice)
    return 0

def writeUpdates(filename, coins, tracker, dbmanager):
    
    global old
    global oldPrices
    global oldPredictions
    global sentimentTenDict
    global prevPrice
    global prevSent
    global sentimentGroup
    global stockTrendGroup
    global averageDiff
    global predictionLast
    prediction = 0
    with open(filename, 'w+') as outfile:
        i = 0
    
        for coin in coins:
            stock_ticker = coin[0]
            tweet_volume = tracker['Volume'][coin[0]]
            tweet_sentiment = tracker['Sentiment'][coin[0]]['avg']
            

            stock_price = tracker['Stock'][coin[0]]
            timestamp = tracker['timestamp'][coin[0]]
            if tweet_sentiment == 'N/A':
                tweet_sentiment = 0.0
            if timestamp == 0:
                timestamp = datetime.now(timezone('US/Eastern'))

            
            trend_value = get_trend(stock_price, coin, len(coins))            
            

            if len(sentimentTenMinDict) < len(coins):
                sentimentTenMinDict[coin[0]] = []
                priceTenMinDict[coin[0]] = []
                sentimentGroup[coin[0]] = []
                stockTrendGroup[coin[0]] = []
                predictionLast[coin[0]] = 0
                old[coin[0]] = 0
                prevSent = tweet_sentiment
                prevPrice = stock_price
            sentimentTenMinDict[coin[0]].append(tweet_sentiment)
            priceTenMinDict[coin[0]].append(stock_price)
            if trend_value != 0:
                sentimentGroup[coin[0]].append(tweet_sentiment)
                stockTrendGroup[coin[0]].append(trend_value)


            if len(sentimentTenMinDict[coin[0]]) == 21:

                fiveMinSent = sentimentTenMinDict[coin[0]][1:11]
                currFiveMinSent = sentimentTenMinDict[coin[0]][11:]
                prevFiveMinDiff = averageDiff[coin[0]][0:10]
                prevAverageSentiment = sum(fiveMinSent)/10
                currAverageSentiment = sum(currFiveMinSent)/10
                prevAverageDiff = sum(prevFiveMinDiff)/10
                averageDiff[coin[0]] = []
                
                priceAverage = sum(priceTenMinDict[coin[0]])/20
                prevPrice = priceTenMinDict[coin[0]][0]
                prevSent = sentimentTenMinDict[coin[0]][0]

                if prevAverageSentiment != 0:
                    x = (prevAverageDiff*currAverageSentiment)/prevAverageSentiment
                    predictionLast[coin[0]] = stock_price + x
                    print("Predicted value:    ", predictionLast[coin[0]])
                else:
                    x = 0
                    predictionLast[coin[0]] = 0

                
        
                old[coin[0]] = 1

                
                

                priceTenMinDict[coin[0]] = []
                sentimentTenMinDict[coin[0]] = []
            else:
                prediction = stock_price
            
            if old[coin[0]] >= 1 and old[coin[0]] <=10:
                prediction = stock_price +(old[coin[0]]*((predictionLast[coin[0]] - stock_price)/10))
                if old[coin[0]] == 10:
                    old[coin[0]] = 0
                else:
                    old[coin[0]] = old[coin[0]]+1

             
            print("Trend Value: ", trend_value)
            
            outfile.write("%s,%d,%s,%s\n" % (stock_ticker, tweet_volume, tweet_sentiment, stock_price))
            if tweet_volume != 0 and stock_price !=0:
                dbmanager.insertStream("stream_data",tweet_volume, tweet_sentiment , stock_price , stock_ticker , timestamp, trend_value, prediction)
            outfile.flush()


def elapsedTime(start):
    return (time.time() - start)


def process(text):
    text = re.sub("[0-9]+", "number", text)
    text = re.sub("#", "", text)
    text = re.sub("\n", "", text)
    text = re.sub("$[^\s]+", "", text)
    text = re.sub("@[^\s]+", "", text)
    text = re.sub("(http|https)://[^\s]*", "", text)
    text = re.sub("[^\s]+@[^\s]+", "", text)
    #text = re.sub('[^a-z A-Z]+', '', text)
    
    return text


class CoinListener(tweepy.StreamListener):

    def __init__(self, auth, coins, queries, refresh, path, dbmanager, realtime=False, logTracker=True, logTweets=True,logSentiment=False, debug=True):

        self.api = tweepy.API(auth)
        self.coins = coins
        self.queries = queries
        self.refresh = refresh
        self.path = path
        self.realtime = realtime
        self.logTracker = logTracker
        self.logTweets = logTweets
        self.logSentiment = logSentiment
        self.debug = debug
        self.processing = False
        self.timer = time.time()
        self.reversal = getReversal(coins)
        self.tracker = getTracker(coins)
        self.dbmanager = dbmanager

        # Initiate file for visualization
        if self.realtime:
            writeUpdates(('%supdates.txt' % self.path), self.coins, self.tracker, self.dbmanager)

    def process(self):
        # Record timer and reset
        self.tracker['Seconds'] += elapsedTime(self.timer)
        self.timer = time.time()

        # Copy tracking data to temporary tracker
        tempTracker = copy.deepcopy(self.tracker)
        self.tracker = getTracker(self.coins)

        # Calculate average sentiment
        if self.logSentiment:
            for coin in self.coins:
                try:
                     retweets = tempTracker['Sentiment'][coin[0]]['retweets']
                     followers = tempTracker['Sentiment'][coin[0]]['followers']
                     combined = [x+y for x,y in zip(retweets, followers)]

                     tempTracker['Sentiment'][coin[0]]['avg'] = np.average(
                        tempTracker['Sentiment'][coin[0]]['all'], weights = combined
                     )
                     tempTracker['Sentiment'][coin[0]]['avg2'] = round(
                        sum(tempTracker['Sentiment'][coin[0]]['all']) / len(tempTracker['Sentiment'][coin[0]]['all']),
                        2)
                except ZeroDivisionError:
                    tempTracker['Sentiment'][coin[0]]['avg'] = 'N/A'

        # Data logging
        if self.logTracker:
            for coin in self.coins:
                with open("%s%s_Tracker.txt" % (self.path, coin[0]), "a") as outfile:
                    outfile.write("%s,%d,%s,%d\n" % (time.strftime('%m/%d/%Y %H:%M:%S'), tempTracker['Volume'][coin[0]],
                                                     tempTracker['Sentiment'][coin[0]]['avg'], tempTracker['Seconds']))

        # Data visualization
        if self.realtime:

            #######

            for coin in self.coins:

                rsp = requests.get('https://finance.google.com/finance?q=' + coin[0] + '&output=json')
                print('https://finance.google.com/finance?q=' + coin[0])
                if rsp.status_code in (200,):

                    fin_data = json.loads(rsp.content[6:-2].decode('unicode_escape'))
                    price = float(fin_data['l'].replace(',', ''))
                    print(price)
                    tempTracker['Stock'][coin[0]] = price
                    tempTracker['timestamp'][coin[0]] = datetime.now(timezone('US/Eastern'))


                    #######


            writeUpdates(('%supdates.txt' % self.path), self.coins, tempTracker, self.dbmanager)


        # Print to console
        if self.debug:
            print("---%s---" % time.strftime('%H:%M:%S'))
            for coin in self.coins:
                print("%s Volume: %s" % (coin[0], tempTracker['Volume'][coin[0]]))
                print("%s Sentiment: %s" % (coin[0], tempTracker['Sentiment'][coin[0]]['avg']))
            print('\n')

        self.processing = False

    def on_status(self, status):
        tweetOrgnl = status.text
        tweetLower = tweetOrgnl.lower()
        retweets = status.retweet_count
        followers = status.user.followers_count

        # For every incoming tweet...
        for query in self.queries:
            if query.lower() in tweetLower:

                # Categorize tweet
                lookup = self.reversal[query]

                # Increment count
                self.tracker['Volume'][lookup] += 1

                # Sentiment analysis
                if self.logSentiment:
                    tweetPrcsd = process(tweetLower)
                    tweetScore = SentimentIntensityAnalyzer().polarity_scores(tweetPrcsd)["compound"]
                    #print("Tweet: ", tweetLower, "Score: ", tweetScore)
                    if tweetScore != 0.0:
                        self.tracker['Sentiment'][lookup]['all'].append(tweetScore)
                        self.tracker['Sentiment'][lookup]['retweets'].append(retweets)
                        self.tracker['Sentiment'][lookup]['followers'].append(followers)
                    
                else:
                    tweetScore = 'N/A'

                # Log tweet
                if self.logTweets:
                    with codecs.open("%s%s_Tweets.txt" % (self.path, lookup), "a", encoding='utf8') as outfile:
                        outfile.write("%s,%s,%s\n" % (time.strftime('%m/%d/%Y %H:%M:%S'), tweetOrgnl, tweetScore))

                # Check refresh
                if elapsedTime(self.timer) >= self.refresh:
                    if not self.processing:
                        self.processing = True
                        processingThread = threading.Thread(target=self.process)
                        processingThread.start()
        return True

    def on_error(self, status_code):
        if status_code == 413 or status_code == 420 or status_code == 503:
            print("Got an error with status code: %d" % status_code)
            with open("%sError_Log.txt" % self.path, "a") as outfile:
                outfile.write("%s Error: %d\n" % (time.strftime('%m/%d/%Y %H:%M'), status_code))
            return False
        print('Got an error with status code: %d' % status_code)
        return True  # To continue listening

    def on_timeout(self):
        print("Timeout...")
        return True  # To continue listening

# Update Database ---------------------------------------
class PostgreSQLStreamManager():

    def __init__(self, conn):
        self.conn = conn


    def insertStream(self, table, volume, sentiment, price, stock, timestamp, trend_value, prediction):
        cur = self.conn.cursor()
    
        query =  """
		  INSERT INTO {} (time, stack_name, price, tweet_volume, sentiment, stock_trend, prediction) VALUES(
		\'{}\',
		\'{}\',
		\'{}\',
		\'{}\',
		\'{}\',
		\'{}\',
		{});""".format(table, timestamp, stock, price, volume, sentiment, trend_value, prediction)
        cur.execute(query)
        self.conn.commit()




# Streaming --------------------------------------------------

def streaming(credentials, coins, queries, refresh, path, conn, realtime=False, logTracker=True, logTweets=True,
              logSentiment=False, debug=True):
    # User Error Checks
    if len(coins) <= 0:  print("Error: You must include at least one coin."); return
    if len(coins) >= 10: print("Warning: Fewer than ten coins recommended.")
    if len(queries) <= 0:  print("Error: You must include at least one query."); return
    if len(queries) >= 20: print("Warning: Fewer than twenty queries recommended.")
    if refresh <= 0:  print("Error: Refresh rate must be greater than 0"); return

    auth = tweepy.OAuthHandler(credentials[0], credentials[1])
    auth.set_access_token(credentials[2], credentials[3])
    dbmanager = PostgreSQLStreamManager(conn)

    if logSentiment:
        global SentimentIntensityAnalyzer
        from nltk.sentiment.vader import SentimentIntensityAnalyzer

    while True:

        # Start streaming -----------------------------
        try:
            print("Streaming Now...")
            listener = CoinListener(auth, coins, queries, refresh, path,dbmanager, realtime, logTracker, logTweets, logSentiment,debug)
            stream = tweepy.Stream(auth, listener)
            stream.filter(track=queries, languages = ['en'])

        except (Timeout, ConnectionError, ReadTimeoutError):
            print("Reestablishing Connection...")
            with open("%sError_Log.txt" % path, "a") as outfile:
                outfile.write("%s Error: Connection Dropped\n" % time.strftime('%m/%d/%Y %H:%M'))

        time.sleep((15 * 60) + 1)  # Wait at least 15 minutes before restarting listener

        # --------------------------------------------


# consumer key, consumer secret, access token, access secret.
ckey = "NitBuWXj9MYqRmhxJYSM1OeMk"
csecret = "2UUUnXXesBxL9hujHZg8axacJrY5zChtDHkWynawyj5AB6Nt8D"
atoken = "348867799-IXepXIu6M5Hbipw0B1kUip3YcfU0R1jsiCcrU8yp"
asecret = "PbBXHCXaX953glndUpA2uWgMpCOoqBMfIvmyZp1rg6F3t"

credentials = [ckey, csecret, atoken, asecret]

# First element must be ticker/name, proceeding elements are extra querie
TSLA = ['TSLA', 'Tesla', 'tesla', 'tsla', 'gigafactory', 'elonmusk']
DIS = ['DIS', 'Disney']
NKE = ['NKE', 'Nike', 'JustDoIt']
AMZN = ['AMZN', 'Amazon', 'Bezos', 'Jeff Bezos', 'aws', 'amazon cloud services']
# Variables
tickers = [TSLA, DIS, NKE, AMZN]  # Used for identification purposes
queries = TSLA + DIS + NKE + AMZN  # Filters tweets containing one or more query
refresh = 30  # Process and log data every 30 seconds

# Create a folder to collect logs and temporary files also establish connection to database
conn = psycopg2.connect("dbname=streaming user='ec2-user'")
path = "/home/ec2-user/streamingdata/tweet_logs/"
streaming(credentials, tickers, queries, refresh, path, conn=conn, realtime=True, logTracker=True, logTweets=True,
                  logSentiment=True, debug=True)
