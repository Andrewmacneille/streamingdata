from bokeh.layouts import row, column, widgetbox
from bokeh.models.widgets import TextInput
from pytz import timezone
from bokeh.plotting import figure, curdoc
from bokeh.models import Span
from bokeh.palettes import Plasma5, Accent3
from bokeh.models import LinearAxis, Range1d, DatetimeTickFormatter, DataRange1d, CustomJS, Rect
from bokeh.models.widgets import Button
from bokeh.driving import count
from bokeh.models.tools import PanTool, BoxZoomTool, WheelZoomTool, ResetTool, HoverTool, LassoSelectTool, BoxSelectTool, UndoTool
from bokeh.models.sources import ColumnDataSource
from bokeh.io import show
import random
import numpy as np
import pandas as pd
from datetime import datetime as dt
from datetime import timedelta
import psycopg2

prevVolume = 0
prevPrice = 0
ticker_textbox = TextInput(placeholder="Ticker")

TICKER = ''


#
# Method used in the testing of application to read metrics from a flat file and display them via bokeh
#

def readUpdates(filename):
    updates = {}
    with open(filename, 'r') as infile:
        for line in infile:
            l = line.strip('\n').split(',')
            volume = int(l[1])
            sentiment = 0
            if l[2] != 'N/A': sentiment = float(l[2])
            stock = float(l[3])
            updates[l[0]] = (volume, sentiment, stock)
    return updates

#
# Since we only are offered 10% of the twitter stream, we extrapolate the volume metric to obtain a more accurate value
#

def extrapolate(tweets):
    return tweets * 99 + random.randint(0, 99)

#
# Configures a bokeh figure for each respective stock ticker
#

def getPlot(title):
    source = ColumnDataSource(data=dict(x=[], y=[], width=[], height=[]))

    callback = CustomJS(args=dict(source=source), code="""
        // get data source from Callback args
        var data = source.data;

        /// get BoxSelectTool dimensions from cb_data parameter of Callback
        var geometry = cb_data['geometry'];

        /// calculate Rect attributes
        var width = geometry['x1'] - geometry['x0'];
        var height = geometry['y1'] - geometry['y0'];
        var x = geometry['x0'] + width/2;
        var y = geometry['y0'] + height/2;

        /// update data source with new Rect attributes
        data['x'].push(x);
        data['y'].push(y);
        data['width'].push(width);
        data['height'].push(height);

        // emit update of data source
        source.change.emit();
    """)


    rect = Rect(x='x',
            y='y',
            width='width',
            height='height',
            fill_alpha=0.3,
            fill_color='#009933')


    tools = [PanTool(), BoxZoomTool(), ResetTool(), WheelZoomTool(), BoxSelectTool(callback=callback), UndoTool()]
    p = figure(plot_height=350, plot_width=700, min_border=40, toolbar_location='above',tools = tools, title=title)
    p.background_fill_color = "#fafafa"  # Background color#515052
    p.title.text_color = "#333138"  # Title color
    p.title.text_font = "helvetica"  # Title font
    p.xaxis[0].formatter = DatetimeTickFormatter(
        microseconds=["%d %B %Y %H:%M:%S "],
        seconds=["%d %B %Y %H:%M:%S"],
        minsec=["%d %B %Y %H:%M:%S"],
        minutes=["%d %B %Y %H:%M:%S"],
        hours=["%d %B %Y %H:%M:%S"],
        days=["%d %B %Y %H:%M:%S"],
        months=["%d %B %Y %H:%M:%S"],
        years=["%d %B %Y %H:%M:%S "],
    )
    p.xaxis.major_label_orientation = 3/4
    p.xaxis.major_tick_line_color = None  # Turn off x-axis major ticks
    p.xaxis.minor_tick_line_color = None  # Turn off x-axis minor ticks
    p.yaxis.major_tick_line_color = None  # Turn off y-axis major ticks
    p.yaxis.minor_tick_line_color = None  # Turn off y-axis minor ticks
    p.xgrid.grid_line_alpha = 0  # Hide X-Axis grid
    p.ygrid.grid_line_alpha = 0  # Hide Y-Axis grid
    p.xaxis.major_label_text_color = "#333138"  # X-Axis color
    p.yaxis.major_label_text_color = "#333138"  # Y-Axis color
    p.select(BoxSelectTool).select_every_mousemove = False
    p.add_glyph(source, rect, selection_glyph=rect, nonselection_glyph=rect)
    return p


#
# visualize is called with the specific tickers that are going to be viewed and queries the postGres database for the correct range of columns
#

def visualize(coins, seconds, path, conn):
    milliseconds = seconds * 1000
    plots = []
    global prevVolume, prevPrice, ticker_textbox
    plotLine = []
    lines = {}

    #
    # Method to query database, returned is a numpy array of each specific metric
    #

    xs, yPrices, ySentiments, yVolumes,yTrends,yPreds ,unique_names = get_data(coins)
    


    #
    # The numpy arrays are then zipped together so that a tuple containing one metric from each individual numpy array is created.
    #   - One tuple represents a 30 second interval of data
    #   - The data is then appended to a bokeh ColumnDataSource and then plot in the pre configured figure
    #   - Other bokeh add-ons and widgets are appended to the figure in this method such as pan and zoom tools, as well as box zoom functionalities
    #
    #

    for i, (x, yPrice, ySentiment, yVolume,yTrend,yPred,name) in enumerate(zip(xs, yPrices, ySentiments, yVolumes,yTrends,yPreds, unique_names)):

        
            priceSource = ColumnDataSource(dict(x=x,
                                                y=yPrice,
                            timestamp = ['{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour, a.minute, a.second) for a in x],
                            stock_name=[name]*len(x)))

            sentimentSource = ColumnDataSource(dict(x=x,
                                                y=ySentiment,
                                                timestamp=[
                                                    '{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,
                                                                                           a.minute, a.second) for a in x],
                                                stock_name=[name] * len(x)))

            volumeSource = ColumnDataSource(dict(x=x,
                                                y=yVolume,
                                                timestamp=[
                                                    '{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,
                                                                                           a.minute, a.second) for a in x],
                                                stock_name=[name] * len(x)))

            trendSource = ColumnDataSource(dict(x=x,
                                                y=yTrend,
                                                timestamp=[
                                                    '{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,
                                                                                           a.minute, a.second) for a in x],
                                                stock_name=[name] * len(x)))
            predSource = ColumnDataSource(dict(x=x,
                                                y=yPred,
                                                timestamp=[
                                                    '{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,
                                                                                           a.minute, a.second) for a in x],
                                                stock_name=[name] * len(x)))

            plot = []
            plotRight = []
            minPrice = yPrice.min()-2
            print(name)
            print(len(yVolume))
            minVolume = yVolume.min()-5
            if minPrice <= 0:
                if prevPrice != 0:
                    minPrice = prevPrice
                else:
                    minPrice = yPrice.max()-5
            else:
                prevPrice = minPrice

            if minVolume <=0:
                #if prevVolume != 0:
                minVolume = prevVolume
                #else:
                #    minVolume = yVolume.max()-5
            else:
                prevVolume = minVolume

            pltRight = getPlot(name)
            plt = getPlot(name)
            plt.y_range=(Range1d(start=ySentiment.min()-0.5, end = ySentiment.max()+0.5))

            plot.append(plt)

            plot.append(plot[0].line(x = 'x', y = 'y', line_alpha=0.5, color="red", line_width=1,muted_color=Accent3[0], muted_alpha=0.2, legend="Sentiment", source = sentimentSource))
            plot.append(plot[1].data_source)

            plot.append(plot[0].line(x='x',y='y', line_alpha=0.7, color='orange' ,muted_color=Plasma5[0], muted_alpha=0.2, legend="Stock Trend", source = trendSource))
            plot.append(plot[3].data_source)

            plotRight.append(pltRight)
            plotRight.append(plotRight[0].line(x = 'x', y = 'y', line_alpha=0.5, color="green", line_width=1,muted_color=Plasma5[2], muted_alpha=0.2, legend="Stock", source = priceSource))
            plotRight.append(plotRight[1].data_source)
            plotRight.append(plotRight[0].line(x = 'x', y = 'y', line_alpha=0.5, color="blue", line_width=1,muted_color=Plasma5[2], muted_alpha=0.2, legend="Prediction", source = predSource))
            plotRight.append(plotRight[3].data_source)
            now = dt.now()
            time_now = now + timedelta(hours = -5)
            time_now_10 = time_now + timedelta(minutes = -10)
            interval_start = Span(location = time_now_10.timestamp()*1000, dimension='height', line_color = 'green', line_dash='dashed', line_width=0.5)
            interval_end = Span(location = time_now.timestamp()*1000, dimension='height', line_color = 'green', line_dash='dashed', line_width=0.5)
            plotRight.append(interval_start)
            plotRight.append(interval_end)
            plotRight[0].add_layout(interval_start)
            plotRight[0].add_layout(interval_end)
            plot[0].legend.location = "top_left"
            plot[0].legend.click_policy="mute"
            plotRight[0].legend.location = "top_left"
            plotRight[0].legend.click_policy="mute"
            hover1 = HoverTool(
            tooltips=[
                ( 'Time Interval', '@x{%d %B %Y %H:%M:%S}')
             #   ( 'Volume of Tweets', '@y')
            ],

            formatters={
                'x'      : 'datetime', # use 'datetime' formatter for 'date' field
            },
            renderers=[plot[1]],
            # display a tooltip whenever the cursor is vertically in line with a glyph
            mode='vline'
            )
            hover2 = HoverTool(
            tooltips=[
                ( 'Sentiment', '@y{%0.2}')
            ],

            formatters={
                'y'     : 'printf'
            },
            renderers=[plot[3]],
            # display a tooltip whenever the cursor is vertically in line with a glyph
            mode='vline'
            )
            hover3 = HoverTool(
            tooltips=[
                ( 'Price', '$@y{%0.2f}')
            ],

            formatters={
                'y'     : 'printf'
            },
            #renderers=[plot[5]],
            # display a tooltip whenever the cursor is vertically in line with a glyph
            mode='vline'
            )

            update = Button(label="Update")
            update.on_click(update_ticker)
            inputs = widgetbox([ticker_textbox, update], width=200)

            plot[0].tools.append(hover1)
            #plot[0].tools.append(hover2)
            #plot[0].tools.append(hover3)
            plots.append(plotRight)
            plots.append(plot)


    #
    #
    #   The call to visualize above is only good for the requested previous group of data. In order to view the stream data in real time a bokeh call back
    #   is configured to re-query the database every 30 seconds, which is the same as the frequency the data is being streamed in
    #       - The method is the same, however, it is slightly inefficient. Every 30 seconds we request the entire previous segment of data that was requested
    #           on the first call to visualize and simply replace the old data. There are methods in bokeh that can be used to only update data that is not currently
    #           present in the ColumnDataSource strucutre, however, there are bugs in the code


    #@count()
    def update():
        # Apply updates
        xs, yPrices, ySentiments, yVolumes,yTrends ,yPreds,unique_names = get_data(coins)  

        for i, (x, yPrice, ySentiment, yVolume,yTrend,yPred, name) in enumerate(zip(xs, yPrices, ySentiments, yVolumes,yTrends,yPreds, unique_names)):

            i = i*2


            
            plots[i+1][2].data =dict(x=x,
                        y= ySentiment,
                        timestamp=['{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,a.minute,a.second) for a in x],
                        stock_name=[name]*len(x))
            plots[i+1][4].data =dict(x=x,
                        y= yTrend,
                        timestamp=['{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,a.minute,a.second) for a in x],
                        stock_name=[name]*len(x))
            plots[i][2].data =dict(x=x,
                        y= yPrice,
                        timestamp=['{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,a.minute,a.second) for a in x],
                        stock_name=[name]*len(x))
            plots[i][4].data =dict(x=x,
                        y= yPred,
                        timestamp=['{}/{}/{} {:02d}:{:02d}:{:02d}'.format(a.month, a.day, a.year, a.hour,a.minute,a.second) for a in x],
                        stock_name=[name]*len(x))

            




    rows = []
    for i in range(0, len(plots), 1):
        try:
            rows.append(row(plots[i][0]))
            
        except IndexError:
            rows.append(row(plots[i][0]))

    print("BEFORE ASSIGNING CURR ROOR\n", rows[0])
    root = column([r for r in rows])
    curdoc().clear()
    curdoc().add_root(row(inputs, root))
    curdoc().add_periodic_callback(update, milliseconds)



def get_data(coins):
    """
    helper function to return stock data from last  x amount of time
    """
    df = pd.read_sql("""
	SELECT * FROM stream_data order by time desc limit 120;
	""", conn)


    unique_names = []
    grouped = df.groupby('stack_name')
    unique_names_bulk = df.stack_name.unique()
    for name in unique_names_bulk:
        
        if name in coins:
            print(name)
            unique_names.append(name)
    print("visualize names: ", len(unique_names))
   
    yPrices = [grouped.get_group(stock)['price'] for stock in unique_names]
    ySentiments = [grouped.get_group(stock)['sentiment'] for stock in unique_names]
    yVolumes = [grouped.get_group(stock)['tweet_volume'] for stock in unique_names]
    yTrends = [grouped.get_group(stock)['stock_trend'] for stock in unique_names]
    yPreds = [grouped.get_group(stock)['prediction'] for stock in unique_names]
    xs = [grouped.get_group(stock)['time'] for stock in unique_names]

    return (xs, yPrices, ySentiments,yVolumes,yTrends,yPreds, unique_names)



# Make sure these variables are consistent with streaming.py
tickers = ['TSLA','AMZN','NKE','DIS']
refresh = 30
path = "/home/ec2-user/streamingdata/tweet_logs/"

conn = psycopg2.connect("dbname=streaming user='ec2-user'")
def update_ticker():
    global TICKER
    TICKER = ticker_textbox.value
    print(TICKER)
    visualize([TICKER], refresh, path, conn)
    return

visualize(tickers, refresh, path, conn)
            

