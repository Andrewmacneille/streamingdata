Code Documentation

Supplementary Video link: https://www.youtube.com/watch?v=rE_LM_r1HgQ&feature=youtu.be   PLEASE SKIP TO TIME: 2:35


There are three main components that make up our application, Python data fetcher, PostgreSQL database and Bokeh visualization application. The data fetcher uses Twitter�s Streaming API and Google�s Finance API to retrieve specific metrics that we collect and enter in the Postgres database. On connection to our bokeh application, these metrics are viewable via a python Bokeh figure.

Twitter:

-A Listener Class is given our open stream connection with Twitter�s API and on the event of a tweet that contains one of the specified key words it parses the tweet, increments a counter for that specific stock ticker and assigns a sentiment value towards the tweet. 

-Sentiment is calculated by first stripping the tweet of mentions (@) and URLS, however, emoticons and punctuation are left

-This is because VADER�s lexicon accounts for such extra punctuation and emoticon use and applies updates to their sentiment analysis accordingly

-The stream continues collecting sentiment for 30 seconds, when that time is up the avg sentiment over the time period is calculated, along with the total volume of tweets. 

-When the 30 seconds has concluded the finance api is connected with and the most recent price for a particular stock ticker is retrieved. 

-All of these values are then inserted into our PostgreSQL database. 

Bokeh Visualization:

-We specify a specific time range to create a query for selecting a table of data from our database to present in our visualization

-This data frame is given to a Bokeh figure and a initial visualization is created. 

-Since every 30 seconds data is inserted into the DB, every 30 seconds the streaming visual is updated. 



How To Use Tool

Application Link: http://52.72.63.48:5001/visualize_stocks

The application is essentially a visualization guide for anyone trying to view the relationship between stock prices and their relative sentiment on social media, in this case twitter. 

You can type in a specific stock ticker to view the individual metrics of that particular stock. The metrics are Twitter Sentiment, Stock Price Trend, Stock Price and Stock Price Prediction. 

There is other functionality to allow a user to get a more useable view of the data, such as pan/zoom or box zoom. These widget can be found in the upper left hand tool box on every figure shown. 

Every 30 seconds the views are updated with realtime data 
