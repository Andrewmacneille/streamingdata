#!/bin/bash
nohup python3.6 streaming.py  </dev/null >/dev/null 2>&1 &
nohup python3.6 -m bokeh serve --show --port 5001 visualize_stocks.py --allow-websocket-origin=52.72.63.48:5001 </dev/null >/dev/null 2>&1 &
